from sc2 import race_townhalls
from sc2.client import *
from sc2 import race_worker
import sc2
from sc2.constants import *
import random
import os
import configparser

class WarManager():
    def __init__(self, bot=None):
        self.bot = bot

        self.bot.commander = False
        self.bot.commanderunit = None
        self.bot.commanderposition = False
        self.bot.commandertarget = None
        self.bot.attack_count = 0
        self.bot.retreat_count = 0
        self.startcounter = False
        self.fightstarted = False
        self.move2ramp = False
        self.bot.expansion = False

        self.enemy_status_older = None
        self.enemy_status = None
        self.my_status_older = None
        self.my_status = None
        self.startcounter = None
        self.attacktime = None

        self.enemy_armypower_older = None
        self.enemy_armypower = None

        self.workerscout = None
        self.bot.scout_tag = None
        self.workerguard = None
        self.mineral_worker = None

    async def Run(self):
        self.bot.all_enemies = []
        self.bot.all_enemies = self.bot.all_enemies + self.bot.enemy_units
        self.bot.all_enemies = self.bot.all_enemies + self.bot.enemy_structures

        await self.saveAttacktime()

        if self.bot.iteration & 2 == 0:
            await self.estimate()
            await self.estimate_status()

        #await self.check_retreat()
        await self.counterattack()
        await self.commanderroutine()
        await self.fightroutine()
        await self.counterworkerrush()
        await self.countercannonrush()
        await self.defendroutine()

        if self.bot.showstatus:
            if int(self.bot.time) & 2 == 0:
                self.bot.gamemanager.clearscreen()
                print("My Army:             " + str(self.my_armypower) + "/" + str(self.bot.attack_count))
                print("My Army (-30s):      " + str(self.my_armypower_older))
                print("My Status:           " + self.my_status)
                print("My Status (-30s):    " + self.my_status_older)

                print("Enemy Army:          " + str(self.enemy_armypower))
                print("Enemy Army (-30s):   " + str(self.enemy_armypower_older))
                print("Enemy Status:        " + self.enemy_status)
                print("Enemy Status (-30s): " + self.enemy_status_older)

    async def saveAttacktime(self):
        """Checks when 1st attack is made and writes to datafile"""
        if not self.attacktime:
            enemies = self.bot.enemy_units
            if enemies:
                closeenemies = enemies.closer_than(20, self.bot.main_base_ramp.top_center.position).filter(lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE] and w.type_id not in [UnitTypeId.EGG] and w.type_id not in [UnitTypeId.INTERCEPTOR])
                if closeenemies:
                    self.attacktime = self.bot.time
                    if self.bot.opponent_id:
                        datafile = "./data/" + str(self.bot.opponent_id) + ".db"
                        botdata = configparser.ConfigParser()
                        botdata.read(datafile)
                        if botdata.has_section("Enemy"):
                            botdata.set("Enemy", 'attacktime', str(int(self.attacktime)))
                            botdata.set("Enemy", 'units', str(closeenemies))
                            with open(datafile, 'w') as dbfile:
                                botdata.write(dbfile)

    async def savewinningstrategy(self, result):
        if not result:
            return

        if self.bot.opponent_id:
            if result == Result.Defeat:
                if self.bot.data_race_strategy:
                    datafile = "./data/" + str(self.bot.opponent_id) + ".db"
                    botdata = configparser.ConfigParser()
                    botdata.read(datafile)
                    if botdata.has_section(str(self.bot.race_self)):
                        print("We lost - Removing strategy " + str(self.bot.data_race_strategy))
                        botdata.remove_option(str(self.bot.race_self), 'strategy')
                        botdata.set(str(self.bot.race_self), 'loosing_strategy', self.bot.current_strategy)
                        with open(datafile, 'w') as dbfile:
                            botdata.write(dbfile)

            elif result == Result.Victory:
                datafile = "./data/" + str(self.bot.opponent_id) + ".db"
                botdata = configparser.ConfigParser()
                botdata.read(datafile)
                if botdata.has_section(str(self.bot.race_self)):
                    print("We won - Saving strategy " + str(self.bot.data_race_strategy))
                    botdata.set(str(self.bot.race_self), 'strategy', self.bot.current_strategy)
                    if botdata.has_option(str(self.bot.race_self), "loosing_strategy"):
                        botdata.remove_option(str(self.bot.race_self), 'loosing_strategy')
                    with open(datafile, 'w') as dbfile:
                        botdata.write(dbfile)

    async def estimate(self):
        """Estimates the army value"""
        if int(self.bot.time) & 1 == 0:
            self.my_armypower = self.bot.units.ready.amount - self.bot.workers.ready.amount

        if int(self.bot.time) & 25 == 0:
            self.my_armypower_older = self.my_armypower

        self.enemy_armypower = 0
        enemies = self.bot.enemy_units.not_structure.filter(lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE] and w.type_id not in [UnitTypeId.OVERLORD] and w.type_id not in [UnitTypeId.LARVA] and w.type_id not in [UnitTypeId.EGG] and w.type_id not in [UnitTypeId.INTERCEPTOR])
        for e in enemies:
                self.enemy_armypower = self.enemy_armypower + 1

        if int(self.bot.time) & 25 == 0:
            self.enemy_armypower_older = self.enemy_armypower

        if self.enemy_armypower <= 0:
            self.enemy_armypower = 0

    async def estimate_status(self):
        """Estimates the status"""
        hqpositions = []
        for n in self.bot.townhalls:
            hqpositions.append(n.position)

        enemies = self.bot.enemy_units.not_structure.filter(lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE] and w.type_id not in [UnitTypeId.OVERLORD] and w.type_id not in [UnitTypeId.LARVA] and w.type_id not in [UnitTypeId.EGG] and w.type_id not in [UnitTypeId.INTERCEPTOR])
        enemiesattacking = 0
        for e in enemies:
            for n in hqpositions:
                distance2hq = e.position.distance_to(n)
                if distance2hq <= 40:
                    enemiesattacking = enemiesattacking + 1

        if enemiesattacking >= 5:
            self.enemy_status = 'Attack'
        else:
            self.enemy_status = 'Unknown'

        # Provide output for my own Status
        if self.fightstarted:
            self.my_status = 'Attack'
        elif self.startcounter:
            self.my_status = 'Counter-Attack'
        else:
            self.my_status = 'Gather Units'

        if int(self.bot.time) & 25 == 0:
            self.enemy_status_older = self.enemy_status
            self.my_status_older = self.my_status

    async def counterattack(self):
        if self.bot.time >= 300:
            if (self.enemy_status_older == 'Attack' and self.enemy_status == 'Unknown') and (self.my_status != 'Attack' and self.my_armypower >= 8):
                self.startcounter = True
            elif self.my_armypower <= self.bot.retreat_count:
                self.startcounter = False

    async def check_retreat(self):
        if (self.my_armypower < self.enemy_armypower) or (self.my_armypower <= self.bot.retreat_count):
            self.my_status = 'Retreat'
            fighters = self.bot.units.ready.filter(
                lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [
                    UnitTypeId.OBSERVER] and w.type_id not in [UnitTypeId.WARPPRISM] and w.type_id not in [
                              UnitTypeId.WARPPRISMPHASING] and w.type_id not in [UnitTypeId.OBSERVERSIEGEMODE] and w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE] and w.type_id not in [UnitTypeId.OVERLORD] and w.type_id not in [UnitTypeId.QUEEN] and w.type_id not in [UnitTypeId.LARVA] and w.type_id not in [UnitTypeId.MEDIVAC] and w.type_id not in [UnitTypeId.EGG] and w.type_id not in [UnitTypeId.INTERCEPTOR])

            for fighter in fighters:
                distance2ramp = fighter.position.distance_to(self.bot.ramp_location)
                if distance2ramp >= 5:
                    self.bot.do(fighter.move(self.bot.ramp_location))

    async def countercannonrush(self):
        """Detects Probe rushes - Stupidly"""
        fighters = self.bot.units.filter(lambda w: w.type_id not in [race_worker[self.bot.race]])
        if not self.bot.workers.ready or fighters.amount >= 3:
            return

        if self.bot.all_enemies:
            for enemy in self.bot.all_enemies:
                distance2hq = enemy.position.distance_to(self.bot.hq_location)
                if distance2hq <= 30 and ((enemy.name == 'Pylon') or (enemy.name == 'PhotonCannon')):
                    if self.bot.opponent_id:
                        datafile = "./data/" + str(self.bot.opponent_id) + ".db"
                        botdata = configparser.ConfigParser()
                        botdata.read(datafile)
                        if botdata.has_section("Enemy"):
                            if botdata.has_option("Enemy", "cannonrush"):
                                return
                            else:
                                botdata.set("Enemy", 'cannonrush', "True")
                                with open(datafile, 'w') as dbfile:
                                    botdata.write(dbfile)

                    self.workercount = 0
                    for worker in self.bot.workers:
                        if self.workercount <= 1:
                            self.bot.do(worker.attack(enemy))
                            self.workercount = self.workercount + 1
                            break

    async def counterworkerrush(self):
        fighters = self.bot.units.filter(lambda w: w.type_id not in {race_worker[self.bot.race], UnitTypeId.LARVA, UnitTypeId.OVERLORD, UnitTypeId.EGG})
        if self.bot.enemy_units:
            enemyworker = self.bot.enemy_units.closer_than(30, self.bot.hq_location).filter(lambda w: w.type_id in [UnitTypeId.PROBE] or w.type_id in [UnitTypeId.SCV] or w.type_id in [UnitTypeId.DRONE])
            if enemyworker.amount >= 2 and not fighters:
                if self.bot.opponent_id:
                    datafile = "./data/" + str(self.bot.opponent_id) + ".db"
                    botdata = configparser.ConfigParser()
                    botdata.read(datafile)
                    if botdata.has_section("Enemy"):
                        if not botdata.has_option("Enemy", "workerrush"):
                            botdata.set("Enemy", 'workerrush', "True")
                            with open(datafile, 'w') as dbfile:
                                botdata.write(dbfile)

                counterworkers = self.bot.units(race_worker[self.bot.race]).ready
                mw = self.bot.units.find_by_tag(self.mineral_worker)
                if counterworkers:
                    if mw:
                        counterworkers = counterworkers - [mw]
                    else:
                        self.mineral_worker = counterworkers[0].tag
                        return

                    for counterworker in counterworkers:
                        distance2hq = counterworker.position.distance_to(self.bot.hq_location)
                        if distance2hq <= 30:
                            next_enemy = enemyworker.closest_to(counterworker.position)
                            self.bot.do(counterworker.attack(next_enemy))

        for worker in self.bot.units(race_worker[self.bot.race]).ready:
            distance2hq = worker.position.distance_to(self.bot.hq_location)
            if distance2hq >= 30 and worker.is_attacking:
                self.bot.do(worker.move(self.bot.hq_location))

    async def commanderroutine(self):
        """Defines a Commander"""
        fighters = self.bot.units.ready.filter(lambda w: w.type_id not in self.bot.not_commanderunit)

        if (fighters.amount >= self.bot.attack_count) or self.startcounter:
            self.fightstarted = True
        else:
            self.fightstarted = False

        if fighters:
            if not self.bot.commander:
                    possiblecommanders = sorted(fighters, key=lambda i: i.distance_to(self.bot.enemy_start_locations[0]), reverse=False)
                    self.bot.commander = possiblecommanders[0].tag
            else:
                for fighter in fighters:
                    if fighter.tag == self.bot.commander:
                        self.bot.commanderunit = fighter
                        self.bot.commanderposition = fighter.position
                        #self.bot._client.debug_text_world("C", self.bot.hq_location) # DEBUG
                        await self.bot._client._send_debug()
                        return
                self.bot.commanderunit = None
                self.bot.commander = False
                self.bot.commandertarget = None
                return

    async def defendroutine(self):
        """Reacts to enemies around the HQ"""
        townhall = None
        if self.bot.race_self == 'Protoss':
            townhall = UnitTypeId.NEXUS
        elif self.bot.race_self == 'Terran':
            townhall = UnitTypeId.COMMANDCENTER
        elif self.bot.race_self == 'Zerg':
            townhall = UnitTypeId.HATCHERY

        hqs = self.bot.structures(townhall)
        for hq in hqs:
            if self.bot.enemy_units:
                enemies = self.bot.enemy_units.filter(lambda w: w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE] and w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [UnitTypeId.OVERLORD] and w.type_id not in [UnitTypeId.EGG] and w.type_id not in [UnitTypeId.INTERCEPTOR])

                if enemies:
                    closeenemies = enemies.closer_than(30, hq)
                    if closeenemies:
                        self.attacked = True
                        self.my_status = 'Defend'

                        fighters = self.bot.units.ready.filter(
                            lambda w: w.type_id not in [race_worker[self.bot.race]] and w.type_id not in [
                                UnitTypeId.OBSERVER] and w.type_id not in [UnitTypeId.WARPPRISM] and w.type_id not in [
                                          UnitTypeId.WARPPRISMPHASING] and w.type_id not in [UnitTypeId.OBSERVERSIEGEMODE] and w.type_id not in [UnitTypeId.OVERLORD] and w.type_id not in [UnitTypeId.LARVA]and w.type_id not in [UnitTypeId.MEDIVAC] and w.type_id not in [UnitTypeId.EGG] and w.type_id not in [UnitTypeId.INTERCEPTOR])

                        for fighter in fighters:
                            if self.bot.scout_tag:
                                if self.bot.scout_tag == fighter.tag:
                                    break

                            unit_enemies = self.bot.enemy_units.closer_than(30, hq)
                            for e in unit_enemies:
                                if not self.bot.techtree.can_attack(fighter, e):
                                    unit_enemies.remove(e)
                            if unit_enemies:
                                next_enemy = unit_enemies.closest_to(hq)
                                self.bot.do(fighter.attack(next_enemy.position))
                    else:
                        self.attacked = False

    async def fightroutine(self):
        """The main Attack and Movement Routine"""
        if not self.bot.commander or not self.bot.commanderunit or not self.bot.commanderposition:
            return

        fighters = self.bot.units.ready.filter(lambda w: w.type_id not in self.bot.not_squadunit)
        if fighters:
            for fighter in fighters:

                if self.bot.scout_tag:
                    if fighter.tag == self.bot.scout_tag:
                        break

                # Commander Action
                if fighter.tag == self.bot.commander:

                    if not self.fightstarted:
                        d = random.randint(0, 4)
                        rampdistance = fighter.position.distance_to(self.bot.main_base_ramp.top_center.position)
                        checkpointposition = self.bot.main_base_ramp.top_center.position.towards(self.bot._game_info.player_start_location, d)
                        checkpointdistance = fighter.position.distance_to(checkpointposition)

                        if self.move2ramp:
                            if rampdistance < 2:
                                self.move2ramp = False
                                break
                            else:
                                self.bot.do(fighter.move(self.bot.main_base_ramp.top_center.position.towards(self.bot._game_info.player_start_location, d)))
                        elif not self.move2ramp:
                            if checkpointdistance < 2:
                                self.move2ramp = True
                                break
                            else:
                                if not self.bot.in_pathing_grid(checkpointposition):
                                    return
                                self.bot.do(fighter.move(checkpointposition))

                    elif self.fightstarted:
                        # Target
                        if self.bot.enemy_structures:
                            target = random.choice(self.bot.enemy_structures)
                        elif self.bot.enemy_units:
                            target = random.choice(self.bot.enemy_units)
                        else:
                            target = None
                            await self.scout()

                        squad = self.bot.units.ready.filter(
                            lambda w: w.type_id not in self.bot.not_squadunit).closer_than(10, fighter)

                        if squad.amount <= self.bot.squadsize:
                            self.bot.do(fighter.attack(fighter.position))
                            break

                        if target:
                            self.bot.commanderposition = fighter.position
                            self.bot.commandertarget = target
                            self.bot.do(fighter.attack(target))
                            break
                        else:
                            self.bot.commandertarget = None
                            ramp = self.bot.main_base_ramp.top_center
                            hq = self.bot.townhalls.first
                            p = ramp.position.towards(hq.position, 1)
                            distance = fighter.position.distance_to(p)
                            if distance < 2:
                                break
                            self.bot.do(fighter.attack(p))
                            self.bot.commanderposition = p
                            break

                # Fighter Action
                elif fighter.tag != self.bot.commander:
                    await self.moveroutine(fighter)


    async def moveroutine(self, fighterunit):
        """Orders the given unit to move next to the Commander"""
        if self.bot.scout_tag:
            if fighterunit.tag == self.bot.scout_tag:
                return

        if self.bot.commanderunit:
            distance = fighterunit.position.distance_to(self.bot.commanderposition)
            if distance < 3:
                return
            target = self.bot.commandertarget
            if not target:
                pos = self.bot.commanderposition.towards(fighterunit.position, random.randrange(1, 3))
                if not self.bot.in_pathing_grid(pos):
                    return
            else:
                pos = self.bot.commanderposition.towards(target.position, random.randrange(3, 10))
                if not self.bot.in_pathing_grid(pos):
                    return

            if self.fightstarted:
                self.bot.do(fighterunit.attack(pos))
            else:
                self.bot.do(fighterunit.attack(pos))
        else:
            return

    async def expand(self, force=False):
        townhall = None
        if self.bot.race_self == 'Protoss':
            townhall = UnitTypeId.NEXUS
        elif self.bot.race_self == 'Terran':
            townhall = UnitTypeId.COMMANDCENTER
        elif self.bot.race_self == 'Zerg':
            townhall = UnitTypeId.HATCHERY

        if not force:
            if self.bot.already_pending(townhall):
                self.bot.expansion = False
                return
            if self.bot.can_afford(townhall):
                location = await self.bot.get_next_expansion()
                if location:
                    self.bot.expansion = True
                    await self.bot.build(townhall, location)
        else:
            if self.bot.can_afford(townhall):
                location = await self.bot.get_next_expansion()
                if location:
                    self.bot.expansion = True
                    await self.bot.build(townhall, location)

    async def scout(self, unit=False, corners=False, force=False):
        """Scouts for the enemy"""
        if self.bot.enemy_structures and not force:
            scout = self.bot.units.find_by_tag(self.bot.scout_tag)
            if scout:
                self.bot.do(scout.move(self.bot.townhalls.first.position))
                self.bot.scout_tag = None
                return
            else:
                return

        if not unit:
            unit = self.bot.units.find_by_tag(self.bot.commander)
            if not unit:
                workers = self.bot.units(race_worker[self.bot.race]).ready
                unit = workers[0]
                if not unit:
                    return

        scout = self.bot.units.find_by_tag(self.bot.scout_tag)
        if scout:
            return
        else:
            self.bot.scout_tag = unit.tag
            print("Scout: " + str(unit))
            for enemyStartLoc in list(self.bot.enemy_start_locations):
                if not corners:
                    self.bot.do(unit.move(enemyStartLoc.position, queue=True))
                    self.bot.do(unit.move(self.bot.mineral_field.random.position, queue=True))
                else:
                    if self.bot.startpoint == 'SW':
                        self.bot.do(unit.move(self.bot.mapcorner_se, queue=True))
                        self.bot.do(unit.move(self.bot.mapcorner_ne, queue=True))
                        self.bot.do(unit.move(enemyStartLoc.position, queue=True))
                        self.bot.do(unit.move(self.bot.mineral_field.random.position, queue=True))
                    if self.bot.startpoint == 'SE':
                        self.bot.do(unit.move(self.bot.mapcorner_sw, queue=True))
                        self.bot.do(unit.move(self.bot.mapcorner_nw, queue=True))
                        self.bot.do(unit.move(enemyStartLoc.position, queue=True))
                        self.bot.do(unit.move(self.bot.mineral_field.random.position, queue=True))
                    if self.bot.startpoint == 'NW':
                        self.bot.do(unit.move(self.bot.mapcorner_ne, queue=True))
                        self.bot.do(unit.move(self.bot.mapcorner_se, queue=True))
                        self.bot.do(unit.move(enemyStartLoc.position, queue=True))
                        self.bot.do(unit.move(self.bot.mineral_field.random.position, queue=True))
                    if self.bot.startpoint == 'NE':
                        self.bot.do(unit.move(self.bot.mapcorner_nw, queue=True))
                        self.bot.do(unit.move(self.bot.mapcorner_sw, queue=True))
                        self.bot.do(unit.move(enemyStartLoc.position, queue=True))
                        self.bot.do(unit.move(self.bot.mineral_field.random.position, queue=True))
