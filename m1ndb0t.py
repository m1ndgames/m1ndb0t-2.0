import sc2
from sc2.client import *
from techtree import *
from ressources import *
from gamemanager import *
from unitmanager import *
from warmanager import *
from protoss import *
from terran import *
from zerg import *
from util import *


class m1ndb0t(sc2.BotAI):
    def __init__(self):
        super().__init__()
        self.util = Util(self)
        self.techtree = TechTree(self)
        self.ressources = Ressources(self)
        self.gamemanager = GameManager(self)
        self.unitmanager = UnitManager(self)
        self.protossmanager = ProtossManager(self)
        self.terranmanager = TerranManager(self)
        self.zergmanager = ZergManager(self)
        self.warmanager = WarManager(self)
        self.iteration = None

        # STDOUT
        self.showstatus = False
        self.printdebug = False

        # Chat
        self.usechat = False

    async def on_start(self):
        await self.gamemanager.startGame()

    async def on_step(self, iteration):
        """Runs on every Gamestep"""
        self.iteration = iteration

        # Manage own and enemy Race
        await self.gamemanager.manageRaces()

        await self.gamemanager.setExpansion()

        await self.ressources.manageSupply()

        # Persistent data: Attack Time
        if self.opponent_id:
            datafile = "./data/" + self.opponent_id + ".db"
            if not os.path.isfile(datafile):
                await self.gamemanager.dataAttacktime()
            else:
                await self.gamemanager.readdata(self.opponent_id)

        # Manage Workers
        await self.ressources.manageWorkers()

        # Race Specific Actions
        if self.race_self == 'Protoss':
            await self.protossmanager.Run()
        elif self.race_self == 'Terran':
            await self.terranmanager.Run()
        elif self.race_self == 'Zerg':
            await self.zergmanager.Run()

        # War
        await self.warmanager.Run()

        # Kamikaze
        hq = self.townhalls
        if not hq:
            target = self.enemy_structures.random_or(self.enemy_start_locations[0]).position
            for unit in self.workers | self.units:
                self.do(unit.attack(target))
                return

        if self.printdebug:
            await self.client._send_debug()

    async def on_end(self, game_result):
        await self.warmanager.savewinningstrategy(game_result)
        await self.gamemanager.updatestats(game_result)
