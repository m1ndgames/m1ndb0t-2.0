from sc2.client import *
import random
import os
import configparser
from pathlib import Path
from collections import Counter


class GameManager():
    def __init__(self, bot=None):
        self.bot = bot
        self.ability = self.bot.techtree.techtree['Ability']
        self.unit = self.bot.techtree.techtree['Unit']

        self.bot.unitInfo = {}

        self.bot.real_time = None
        self.bot.last_game_loop = None

        # Race/Enemy specific
        self.bot.race_self = None
        self.bot.race_enemy = None
        self.bot.enemy_id = None

        # Locations
        self.bot.hq_location = None         # 1st townhall
        self.bot.ramp_location = None       # ramp to natural
        self.bot.next_expansion = None      # Next expansion position
        self.bot.rushwait_location = None   # position at natural
        self.bot.proxy_location = None      # proxy position near enemy start position
        self.bot.startpoint = None
        self.bot.mapcorner_sw = None
        self.bot.mapcorner_se = None
        self.bot.mapcorner_nw = None
        self.bot.mapcorner_ne = None
        self.bot.map_center_n = None
        self.bot.map_center_s = None
        self.bot.map_center_w = None
        self.bot.map_center_e = None
        self.bot.map_center = None

        self.attacktime = None
        self.bot.data_attacktime = None
        self.bot.data_attackunits = None
        self.bot.data_race_strategy = None
        self.terran_strategies = []
        self.protoss_strategies = []
        self.zerg_strategies = []
        self.bot.not_squadunit = []
        self.bot.not_commanderunit = []

    def clearscreen(self):
        if os.name == 'nt':
            _ = os.system('cls')
        else:
            _ = os.system('clear')

    async def readdata(self, id):
        botdata = configparser.ConfigParser()
        botdata.read("./data/" + id + ".db")
        if botdata:
            if botdata.has_section("Enemy"):
                if botdata.has_option("Enemy", "attacktime"):
                    self.bot.data_attacktime = botdata.get('Enemy', 'attacktime')
                if botdata.has_option("Enemy", "units"):
                    self.bot.data_attackunits = botdata.get('Enemy', 'units')
            if botdata.has_section(str(self.bot.race_self)):
                if botdata.has_option(str(self.bot.race_self), "strategy"):
                    self.bot.data_race_strategy = botdata.get(str(self.bot.race_self), 'strategy')

    async def updatestats(self, result):
        #if self.bot.opponent_id:
        if not os.path.isfile('./data/stats.txt'):
            Path('./data/stats.txt').touch()
            newstats = configparser.ConfigParser()
            newstats.read('./data/stats.txt')
            newstats.add_section('Stats')
            newstats.set('Stats', 'gamecount', "0")
            newstats.set('Stats', 'wins', "0")
            newstats.set('Stats', 'losses', "0")
            newstats.set('Stats', 'ties', "0")

            newstats.add_section('Terran')
            newstats.set('Terran', 'gamecount', "0")
            newstats.set('Terran', 'wins', "0")
            newstats.set('Terran', 'losses', "0")
            newstats.set('Terran', 'ties', "0")

            newstats.add_section('Zerg')
            newstats.set('Zerg', 'gamecount', "0")
            newstats.set('Zerg', 'wins', "0")
            newstats.set('Zerg', 'losses', "0")
            newstats.set('Zerg', 'ties', "0")

            newstats.add_section('Protoss')
            newstats.set('Protoss', 'gamecount', "0")
            newstats.set('Protoss', 'wins', "0")
            newstats.set('Protoss', 'losses', "0")
            newstats.set('Protoss', 'ties', "0")

            with open('./data/stats.txt', 'w') as statsfile:
                newstats.write(statsfile)

        for file in os.listdir("./data"):
            if file.endswith(".db"):
                datafile = "./data/" + file
                botdata = configparser.ConfigParser()
                botdata.read(datafile)
                if botdata.has_option("Terran", "strategy"):
                    terran_strategy = botdata.get('Terran', 'strategy')
                    self.terran_strategies.append(terran_strategy)
                if botdata.has_option("Protoss", "strategy"):
                    protoss_strategy = botdata.get('Protoss', 'strategy')
                    self.protoss_strategies.append(protoss_strategy)
                if botdata.has_option("Zerg", "strategy"):
                    zerg_strategy = botdata.get('Zerg', 'strategy')
                    self.zerg_strategies.append(zerg_strategy)

        terran_strategy_occurence = Counter(self.terran_strategies).most_common(1)
        protoss_strategy_occurence = Counter(self.protoss_strategies).most_common(1)
        zerg_strategy_occurence = Counter(self.zerg_strategies).most_common(1)

        statsupdate = configparser.ConfigParser()
        statsupdate.read('./data/stats.txt')

        statsgamecount = statsupdate.get('Stats', 'gamecount')
        newgamecount = int(statsgamecount) + 1
        statsupdate.set('Stats', 'gamecount', str(newgamecount))

        statsracegamecount = statsupdate.get(str(self.bot.race_self), 'gamecount')
        newracegamecount = int(statsracegamecount) + 1
        statsupdate.set(str(self.bot.race_self), 'gamecount', str(newracegamecount))

        if terran_strategy_occurence:
            statsupdate.set('Terran', 'best_strategy', str(terran_strategy_occurence[0][0]))
        if protoss_strategy_occurence:
            statsupdate.set('Protoss', 'best_strategy', str(protoss_strategy_occurence[0][0]))
        if zerg_strategy_occurence:
            statsupdate.set('Zerg', 'best_strategy', str(zerg_strategy_occurence[0][0]))

        if result == Result.Victory:
            wingamecount = statsupdate.get(str(self.bot.race_self), 'wins')
            newwingamecount = int(wingamecount) + 1
            statsupdate.set(str(self.bot.race_self), 'wins', str(newwingamecount))
        elif result == Result.Defeat:
            defeatgamecount = statsupdate.get(str(self.bot.race_self), 'losses')
            newdefeatgamecount = int(defeatgamecount) + 1
            statsupdate.set(str(self.bot.race_self), 'losses', str(newdefeatgamecount))
        elif result == Result.Tie:
            tiegamecount = statsupdate.get(str(self.bot.race_self), 'ties')
            newtiegamecount = int(tiegamecount) + 1
            statsupdate.set(str(self.bot.race_self), 'ties', str(newtiegamecount))

        with open('./data/stats.txt', 'w') as statsfile:
            statsupdate.write(statsfile)

    async def startGame(self):
        if not self.bot.real_time and self.bot.last_game_loop == self.bot.state.game_loop:
            self.bot.real_time = True
            self.bot._client.game_step = 1
            return
        elif self.bot.real_time and self.bot.last_game_loop + 3 > self.bot.state.game_loop:
            self.bot._client.game_step = 4
            return

        self.bot.last_game_loop = self.bot.state.game_loop

        self.bot.firstexpansion = await self.bot.get_next_expansion()

        # Set Positions
        await self.set_positions()
        await self.mapbounds()

        if self.bot.opponent_id:
            datafile = "./data/" + str(self.bot.opponent_id) + ".db"
            if not os.path.isfile(datafile):
                Path("./data/" + str(self.bot.opponent_id) + ".db").touch()
                botdata = configparser.ConfigParser()
                botdata.read("./data/" + str(self.bot.opponent_id) + ".db")
                botdata.add_section('Terran')
                botdata.add_section('Protoss')
                botdata.add_section('Zerg')
                botdata.add_section('Enemy')
                with open(datafile, 'w') as dbfile:
                    botdata.write(dbfile)

    async def setExpansion(self):
        self.bot.next_expansion = await self.bot.get_next_expansion()

    async def manageRaces(self):
        # Detect Player and Enemy Race
        if not self.bot.race_self:
            await self.racecheck_self()
        if not self.bot.race_enemy:
            await self.racecheck_enemy()
        if self.bot.race_enemy == 'Unknown':
            await self.racecheck_guessenemy()

    def detect_race(self, query):
        for unit in self.unit:
            if unit['name'] == query:
                return unit['race']

    async def racecheck_self(self):
        """detects played race and sets race specific options"""
        self.bot.race_self = str(self.detect_race(self.bot.townhalls.first.name))
        print("Playing as " + str(self.bot.race_self))

    async def racecheck_enemy(self):
        """detects enemy race and sets race specific options"""
        if self.bot.player_id == 1:
            self.bot.enemy_id = 2
        else:
            self.bot.enemy_id = 1
        self.bot.race_enemy = Race(self.bot._game_info.player_races[self.bot.enemy_id])
        if self.bot.race_enemy == Race.Random:
            self.bot.race_enemy = 'Unknown'
        elif self.bot.race_enemy == Race.Protoss:
            self.bot.race_enemy = 'Protoss'
        elif self.bot.race_enemy == Race.Terran:
            self.bot.race_enemy = 'Terran'
        elif self.bot.race_enemy == Race.Zerg:
            self.bot.race_enemy = 'Zerg'
        print("Enemy Race is " + str(self.bot.race_enemy))

    async def racecheck_guessenemy(self):
        if self.bot.enemy_units:
            for unit in self.bot.enemy_units:
                if self.detect_race(unit.name):
                    unitrace = self.detect_race(unit.name)
                    self.bot.race_enemy = unitrace
                    print("Enemy Race (Random) is " + str(self.bot.race_enemy))

    async def set_positions(self):
        # HQ
        if not self.bot.hq_location:
            self.bot.hq_location = self.bot.townhalls.ready.first.position
            print("Location: HQ = " + str(self.bot.hq_location))

        # Ramp
        if not self.bot.ramp_location:
            pos = self.bot.main_base_ramp.barracks_in_middle
            nx = self.bot.townhalls.ready.first.position
            p = pos.towards(nx, 4)
            self.bot.ramp_location = p
            print("Location: Ramp = " + str(self.bot.ramp_location))
            return

        # Rush wait location
        if not self.bot.rushwait_location:
            self.bot.rushwait_location = await self.bot.get_next_expansion()
            print("Location: Rush wait = " + str(self.bot.rushwait_location))

        # Proxy Position
        if not self.bot.proxy_location:
            if self.bot.known_enemy_structures:
                target = self.bot.known_enemy_structures.first.position
                nx = self.bot.townhalls.ready.first.position
                self.bot.proxy_location = target.towards(nx.position, random.randrange(40, 50))
                print("Location: Proxy Pylon = " + str(self.bot.proxy_location))

    async def mapbounds(self):
        raw_map = self.bot.game_info.playable_area
        map_width = raw_map[2]
        map_height = raw_map[3]
        playerstart = self.bot.game_info.player_start_location

        self.bot.mapcorner_sw = Point2((0, 0))
        self.bot.mapcorner_se = Point2((map_width, 0))
        self.bot.mapcorner_nw = Point2((0, map_height))
        self.bot.mapcorner_ne = Point2((map_width, map_height))
        self.bot.map_center_n = Point2(((map_width / 2), map_height))
        self.bot.map_center_s = Point2(((map_width / 2), 0))
        self.bot.map_center_w = Point2((0, (map_height / 2)))
        self.bot.map_center_e = Point2((map_width, (map_height / 2)))
        self.bot.map_center = Point2(((map_width / 2), (map_height / 2)))

        if playerstart.distance_to(self.bot.mapcorner_sw) < 50:
            self.bot.startpoint = 'SW'
        if playerstart.distance_to(self.bot.mapcorner_se) < 50:
            self.bot.startpoint = 'SE'
        if playerstart.distance_to(self.bot.mapcorner_nw) < 50:
            self.bot.startpoint = 'NW'
        if playerstart.distance_to(self.bot.mapcorner_ne) < 50:
            self.bot.startpoint = 'NE'

    def outofbounds(self, pos):
        if not pos:
            return
        raw_map = self.bot.game_info.playable_area
        map_width = raw_map[2]
        map_height = raw_map[3]

        x = pos[0]
        y = pos[1]

        if (x <= 0) or (x >= map_width):
            return True
        elif (y <= 0) or (y >= map_height):
            return True
