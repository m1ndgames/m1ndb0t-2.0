import random
import os
import configparser
from strategies.terran.MarineRush import *
from strategies.terran.ReaperRush import *

class TerranManager():
    """Protoss Race Manager"""
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.strategy = None
        self.bot.current_strategy = None

    async def Run(self):
        if self.bot.iteration == 1:
            self.bot.not_squadunit.append(race_worker[self.bot.race])
            self.bot.not_commanderunit.append(race_worker[self.bot.race])

        if not self.bot.strategy:
            self.pickStrategy()
        else:
            await self.bot.strategy.Run()

    def setStrategy(self, strategy):
        if strategy:
            strategies = {
                'MarineRush': MarineRush(self.bot),
                'ReaperRush': ReaperRush(self.bot)
            }
            self.bot.strategy = strategies[strategy]


    def pickStrategy(self):
        strategies = {
            'MarineRush': MarineRush(self.bot),
            'ReaperRush': ReaperRush(self.bot)
        }

        # DEBUG Overrides
        # self.bot.strategy = MarineRush(self.bot)
        # self.bot.strategy = ReaperRush(self.bot)
        # return

        if self.bot.data_race_strategy in strategies:
            self.bot.strategy = strategies[self.bot.data_race_strategy]
            print("Using strategy: " + str(self.bot.strategy))
        else:
            if os.path.isfile('./data/stats.txt'):
                statsdata = configparser.ConfigParser()
                statsdata.read('./data/stats.txt')
                if statsdata.has_section(str(self.bot.race_self)):
                    if statsdata.has_option(str(self.bot.race_self), "best_strategy"):
                        best_strategy = statsdata.get(str(self.bot.race_self), 'best_strategy')

                        enemydatafile = "./data/" + str(self.bot.opponent_id) + ".db"
                        if os.path.isfile("./data/" + str(self.bot.opponent_id) + ".db"):
                            enemydata = configparser.ConfigParser()
                            enemydata.read(enemydatafile)
                            if enemydata.has_option(str(self.bot.race_self), "loosing_strategy"):
                                loosingstrategy = enemydata.get(str(self.bot.race_self), 'loosing_strategy')
                                if loosingstrategy == best_strategy:
                                    print(
                                        "Not picking " + best_strategy + " cause we lost with that - Using random strategy")
                                    # TODO: Add a strategy so this is usable
                                    #del strategies[best_strategy]
                                    self.bot.strategy = random.choice(list(strategies.values()))
                                    print("Using strategy:" + str(self.bot.strategy))
                                    return

                        print("No Data - Picking overall best strategy")
                        self.bot.strategy = strategies[best_strategy]
                        return

                print("No Data - Picking random strategy")
                self.bot.strategy = random.choice(list(strategies.values()))
                print("Using strategy:" + str(self.bot.strategy))
