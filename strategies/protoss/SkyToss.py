from sc2.constants import *
from sc2 import race_worker
from protoss_micro import *

class SkyToss:
    """Protoss SkyToss"""
    def __init__(self, bot=None):
        self.bot = bot
        self.ramp_supply_built = False
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.current_strategy = 'SkyToss'
            self.bot.squadsize = 10
            self.bot.attack_count = 1
            self.bot.retreat_count = 0

            self.bot.not_squadunit.extend([UnitTypeId.ADEPT, UnitTypeId.INTERCEPTOR])
            self.bot.not_commanderunit.extend([UnitTypeId.ADEPT, UnitTypeId.MOTHERSHIP, UnitTypeId.INTERCEPTOR])

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: SkyToss", team_only=True)

        if int(self.bot.time) <= 360:
            self.bot.attack_count = 5
            self.bot.retreat_count = 1
        elif int(self.bot.time) > 360:
            self.bot.attack_count = 10
            self.bot.retreat_count = 3
        elif int(self.bot.time) >= 600:
            self.bot.attack_count = 15
            self.bot.retreat_count = 5

        await self.Macro()
        await self.Micro()

    async def Macro(self):
        """Macro Routine"""
        await self.createWall()
        await self.bot.ressources.createGas()
        await self.bot.unitmanager.createBuilding("Stargate", 3, self.bot.hq_location, 15)
        await self.bot.unitmanager.createUnit("Adept", 5)
        await self.bot.unitmanager.createUnit("Carrier", 10)
        await self.bot.unitmanager.createUnit("VoidRay", 10)
        await self.bot.unitmanager.createUnit("Mothership", 1)
        await self.bot.unitmanager.createUnit("Tempest", 3)

        await self.bot.warmanager.scout()

        # Check if expansion is needed
        if (self.bot.units(race_worker[self.bot.race]).ready.amount / 1.1) >= self.bot.neededworkers:
            await self.bot.warmanager.expand()

        if int(self.bot.time) >= 120 and self.bot.townhalls.amount == 1:
            await self.bot.warmanager.expand()
        if int(self.bot.time) >= 360 and self.bot.townhalls.amount <= 3:
            await self.bot.warmanager.expand()

    async def Micro(self):
        """Micro Routine"""
        pm = ProtossMicro(self.bot)

        # Adept
        adepts = self.bot.units(UnitTypeId.ADEPT).ready
        if adepts:
            for adept in adepts:
                await pm.Air(adept, True, True)

        # Voidray
        voidrays = self.bot.units(UnitTypeId.VOIDRAY).ready
        if voidrays:
            for voidray in voidrays:
                await pm.Air(voidray, True, True)

        # Carrier
        carriers = self.bot.units(UnitTypeId.CARRIER).ready
        if carriers:
            for carrier in carriers:
                await pm.Air(carrier)

        # Tempest
        tempests = self.bot.units(UnitTypeId.TEMPEST).ready
        if tempests:
            for tempest in tempests:
                await pm.Air(tempest)

        # Mothership
        motherships = self.bot.units(UnitTypeId.MOTHERSHIP).ready
        if motherships:
            for mothership in motherships:
                if self.bot.commander:
                    if mothership.position.distance_to(self.bot.commanderposition) <= 3:
                        self.bot.do(mothership.move(self.bot.commanderposition))

    async def createWall(self):
        """Wall-in Routine"""
        await self.bot.ressources.createGas()

        rampsupplybuildposition = self.bot.ramp_location
        if self.bot.structures(UnitTypeId.PYLON).ready.exists:
            rampsupply = self.bot.structures.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(5, rampsupplybuildposition)

            if rampsupply:
                self.ramp_supply_built = True
            else:
                self.ramp_supply_built = False
                await self.bot.unitmanager.createSupply(rampsupplybuildposition, 0, True)

            if self.ramp_supply_built:
                depot_placement_positions = self.bot.main_base_ramp.corner_depots | {self.bot.main_base_ramp.depot_in_middle}

                depots = self.bot.structures(UnitTypeId.PHOTONCANNON)

                if depots:
                    depot_placement_positions = {d for d in depot_placement_positions if depots.closest_distance_to(d) > 3}

                if len(depot_placement_positions) == 0:
                    return
                target_depot_location = depot_placement_positions.pop()
                await self.bot.unitmanager.createBuilding("PhotonCannon", 2, target_depot_location, 0, True)

                if depots.ready:
                    await self.bot.unitmanager.createBuilding("ShieldBattery", 1, rampsupplybuildposition, 7, True)