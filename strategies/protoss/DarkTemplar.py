from sc2.constants import *
from sc2 import race_worker
from protoss_micro import *

class DarkTemplar:
    """Protoss DarkTemplar"""
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.current_strategy = 'DarkTemplar'
            self.bot.squadsize = 5
            self.bot.attack_count = 5
            self.bot.retreat_count = 0

            self.bot.not_squadunit.extend([UnitTypeId.ADEPT])
            self.bot.not_commanderunit.extend([UnitTypeId.ADEPT])

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: DarkTemplar", team_only=True)

        await self.Macro()
        await self.Micro()
        await self.Buffs()

    async def Macro(self):
        """Macro Routine"""
        await self.bot.ressources.createGas()
        #await self.createWall()
        await self.bot.unitmanager.createBuilding("DarkShrine", 1, self.bot.hq_location, 20)

        await self.bot.unitmanager.createUnit("Adept", 3)

        if self.bot.townhalls.amount <= 2:
            await self.bot.unitmanager.createBuilding("Gateway", 3, self.bot.hq_location, 20)
        if self.bot.townhalls.amount >= 3:
            await self.bot.unitmanager.createBuilding("Gateway", 4, self.bot.hq_location, 20)

        if self.bot.townhalls.ready.amount >= 3:
            await self.bot.unitmanager.createBuilding("Gateway", self.bot.townhalls.ready.amount, self.bot.hq_location, 30)

        await self.bot.unitmanager.trainSkill("RESEARCH_WARPGATE")

        await self.bot.unitmanager.createUnit("DarkTemplar", 100)

        await self.bot.warmanager.scout()

        # Check if expansion is needed
        if (self.bot.units(race_worker[self.bot.race]).ready.amount / 1.1) >= self.bot.neededworkers:
            await self.bot.warmanager.expand()

        if int(self.bot.time) >= 240 and self.bot.townhalls.amount == 1:
            await self.bot.warmanager.expand()
        if int(self.bot.time) >= 600 and self.bot.townhalls.amount <= 3:
            await self.bot.warmanager.expand()

    async def Micro(self):
        """Micro Routine"""
        pm = ProtossMicro(self.bot)

        # DT Micro
        darktemplars = self.bot.units(UnitTypeId.DARKTEMPLAR).ready
        if darktemplars:
            for darktemplar in darktemplars:
                await pm.Ground(darktemplar)

        # Adept Micro
        adepts = self.bot.units(UnitTypeId.ADEPT).ready
        if adepts:
            for adept in adepts:
                await pm.Ground(adept, True, True)

    async def Buffs(self):
        """Buff Routine"""
        # Morph Gateway to Warpgate if possible
        if self.bot.units(UnitTypeId.GATEWAY).ready:
            gateways = self.bot.units(UnitTypeId.GATEWAY).ready
            for gateway in gateways:
                abilities = await self.bot.get_available_abilities(gateway)
                if AbilityId.MORPH_WARPGATE in abilities and self.bot.can_afford(AbilityId.MORPH_WARPGATE):
                    self.bot.do(gateway(AbilityId.MORPH_WARPGATE))

        # Chronoboost
        for hq in self.bot.townhalls.ready:
            # Boost Cyber Core
            if self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.exists and not self.coreboosted:
                ccore = self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.first
                if not ccore.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.bot.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.bot.do(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, ccore))
                        self.coreboosted = True
                        return

            # Boost Gateway
            if self.bot.units(UnitTypeId.GATEWAY).ready.exists and self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                gw = self.bot.units(UnitTypeId.GATEWAY).ready.first
                if not gw.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.bot.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.bot.do(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, gw))
                        return

            # Boost Warpgate
            if self.bot.units(UnitTypeId.WARPGATE).ready.exists and self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                warps = self.bot.units(UnitTypeId.WARPGATE).ready.first
                if not warps.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.bot.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.bot.do(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, warps))
                        return

    async def createWall(self):
        """Wall-in Routine"""
        await self.bot.ressources.createGas()

        rampsupplybuildposition = self.bot.ramp_location
        if self.bot.structures(UnitTypeId.PYLON).ready.exists:
            rampsupply = self.bot.structures.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(5, rampsupplybuildposition)

            if rampsupply:
                self.ramp_supply_built = True
            else:
                self.ramp_supply_built = False
                await self.bot.unitmanager.createSupply(rampsupplybuildposition, 0, True)

            if self.ramp_supply_built:
                depot_placement_positions = self.bot.main_base_ramp.corner_depots | {self.bot.main_base_ramp.depot_in_middle}

                depots = self.bot.structures(UnitTypeId.PHOTONCANNON)

                if depots:
                    depot_placement_positions = {d for d in depot_placement_positions if depots.closest_distance_to(d) > 3}

                if len(depot_placement_positions) == 0:
                    return
                target_depot_location = depot_placement_positions.pop()
                await self.bot.unitmanager.createBuilding("PhotonCannon", 2, target_depot_location, 0, True)

                if depots.ready:
                    await self.bot.unitmanager.createBuilding("ShieldBattery", 1, rampsupplybuildposition, 7, True)