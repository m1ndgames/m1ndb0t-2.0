from sc2.constants import *
from sc2 import race_worker
from protoss_micro import *


class ZealotRush:
    """Protoss ZealotRush"""
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.current_strategy = 'ZealotRush'
            self.bot.squadsize = 10
            self.bot.attack_count = 1
            self.bot.retreat_count = 0

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: ZealotRush", team_only=True)

        await self.Macro()
        await self.Micro()

    async def Macro(self):
        """Macro Routine"""
        if self.bot.townhalls.amount <= 2:
            await self.bot.unitmanager.createBuilding("Gateway", 3, self.bot.hq_location, 15)
        if self.bot.townhalls.amount >= 3:
            await self.bot.unitmanager.createBuilding("Gateway", 6, self.bot.hq_location, 20)

        if self.bot.townhalls.ready.amount >= 3:
            await self.bot.unitmanager.createBuilding("Gateway", self.bot.townhalls.ready.amount * 2, self.bot.hq_location, 30)

        await self.bot.unitmanager.createUnit("Zealot", 100)

        await self.bot.warmanager.scout()

        # Check if expansion is needed
        if (self.bot.units(race_worker[self.bot.race]).ready.amount / 1.1) >= self.bot.neededworkers:
            await self.bot.warmanager.expand()

        if int(self.bot.time) >= 180 and self.bot.townhalls.amount == 1:
            await self.bot.warmanager.expand()
        if int(self.bot.time) >= 300 and self.bot.townhalls.amount <= 3:
            await self.bot.warmanager.expand()

    async def Micro(self):
        """Micro Routine"""
        pm = ProtossMicro(self.bot)

        # Zealot Micro
        zealots = self.bot.units(UnitTypeId.ZEALOT).ready
        if zealots:
            for zealot in zealots:
                await pm.Ground(zealot)
