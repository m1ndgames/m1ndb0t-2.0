from sc2.position import Point3

from sc2.constants import *
from sc2 import race_worker
from protoss_micro import *

class StalkerRush:
    """Protoss StalkerRush"""
    def __init__(self, bot=None):
        self.bot = bot
        self.ramp_supply_built = False
        self.coreboosted = None
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.current_strategy = 'StalkerRush'
            self.bot.squadsize = 10
            self.bot.attack_count = 10
            self.bot.retreat_count = 3

            self.bot.not_commanderunit.append(UnitTypeId.SENTRY)

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: StalkerRush", team_only=True)

        if int(self.bot.time) <= 360:
            self.bot.attack_count = 5
            self.bot.retreat_count = 1
        elif int(self.bot.time) > 360:
            self.bot.attack_count = 10
            self.bot.retreat_count = 3
        elif int(self.bot.time) >= 600:
            self.bot.attack_count = 15
            self.bot.retreat_count = 5

        await self.Macro()
        await self.Buffs()
        await self.Micro()

    async def Macro(self):
        """Macro Routine"""
        await self.bot.unitmanager.createBuilding("Gateway", 1)

        # Build a wall if enemy is attacking early
        #if self.bot.data_attacktime:
        #    if int(self.bot.data_attacktime) <= 180:
        #        await self.createWall()

        if self.bot.structures(UnitTypeId.GATEWAY) or self.bot.structures(UnitTypeId.WARPGATE):
            await self.bot.ressources.createGas()
            await self.bot.unitmanager.createUnit("Stalker", 50)
            await self.bot.unitmanager.trainSkill("RESEARCH_WARPGATE")

        if self.bot.structures(UnitTypeId.CYBERNETICSCORE):
            await self.bot.unitmanager.createBuilding("Gateway", 3)

        if self.bot.units(UnitTypeId.STALKER):
            await self.bot.unitmanager.trainSkill("RESEARCH_BLINK")
            if self.bot.units(UnitTypeId.STALKER).amount >= 5:
                await self.bot.unitmanager.createUnit("Sentry", 3)

        if self.bot.time >= 120:
            await self.bot.warmanager.scout()

        # Check if expansion is needed
        if (self.bot.units(race_worker[self.bot.race]).ready.amount / 1.1) >= self.bot.neededworkers:
            await self.bot.warmanager.expand()

        if int(self.bot.time) >= 300 and self.bot.townhalls.amount == 1:
            await self.bot.warmanager.expand()
        if int(self.bot.time) >= 600 and self.bot.townhalls.amount <= 3:
            await self.bot.warmanager.expand()

    async def Micro(self):
        """Micro Routine"""
        pm = ProtossMicro(self.bot)

        # Stalker
        stalkers = self.bot.units(UnitTypeId.STALKER).ready
        if stalkers:
            for stalker in stalkers:
                await pm.Ground(stalker, True, True)

        # Sentry
        sentries = self.bot.units(UnitTypeId.SENTRY).ready
        if sentries:
            for sentry in sentries:
                enemies = self.bot.enemy_units
                if enemies:
                    closeenemies = enemies.closer_than(30, sentry)
                    if closeenemies:
                        for e in closeenemies:
                            if not self.bot.techtree.can_attack(sentry, e) or not e.can_be_attacked:
                                closeenemies.remove(e)

                    if not closeenemies:
                        break

                    next_enemy = closeenemies.closest_to(sentry)
                    if not next_enemy:
                        break

                    distance2enemy = sentry.position.distance_to(next_enemy.position)

                    blockpos = self.bot.ramp_location.towards(self.bot.next_expansion, 1)
                    enemydistance2ramp = next_enemy.position.distance_to(self.bot.ramp_location)
                    sentrydistance2ramp = sentry.position.distance_to(self.bot.ramp_location)

                    # Block Ramp
                    if (next_enemy.type_id == UnitTypeId.ZEALOT or next_enemy.type_id == UnitTypeId.MARINE or next_enemy.type_id == UnitTypeId.ZERGLING) and enemydistance2ramp <= 10 and sentry.energy >= 50:
                        self.bot.do(sentry(AbilityId.FORCEFIELD_FORCEFIELD, blockpos))
                        break

                    # Shield
                    if not sentry.has_buff(BuffId.GUARDIANSHIELD) and (sentry.energy >= 75) and (distance2enemy < 8):
                        if (not next_enemy.name == 'Probe') or (not next_enemy.name == 'SCV') or (
                        not next_enemy.name == 'Drone'):
                            self.bot.do(sentry(AbilityId.GUARDIANSHIELD_GUARDIANSHIELD))
                            break
                    else:
                        hqdistance = sentry.position.distance_to(self.bot.hq_location)
                        if (hqdistance < 10) or (distance2enemy < 2):
                            break

                        if distance2enemy < 6:
                            if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                                moveposition = sentry.position.towards(next_enemy.position, -1)
                                if not moveposition or not self.bot.in_pathing_grid(moveposition):
                                    break
                                self.bot.do(sentry.move(moveposition))


    async def Buffs(self):
        """Buff Routine"""
        # Morph Gateway to Warpgate if possible
        if self.bot.units(UnitTypeId.GATEWAY).ready:
            gateways = self.bot.units(UnitTypeId.GATEWAY).ready
            for gateway in gateways:
                abilities = await self.bot.get_available_abilities(gateway)
                if AbilityId.MORPH_WARPGATE in abilities and self.bot.can_afford(AbilityId.MORPH_WARPGATE):
                    self.bot.do(gateway(AbilityId.MORPH_WARPGATE))

        # Chronoboost
        for hq in self.bot.townhalls.ready:
            # Boost Cyber Core
            if self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.exists and not self.coreboosted:
                ccore = self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.first
                if not ccore.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.bot.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.bot.do(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, ccore))
                        self.coreboosted = True
                        return

            # Boost Gateway
            if self.bot.units(UnitTypeId.GATEWAY).ready.exists and self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                gw = self.bot.units(UnitTypeId.GATEWAY).ready.first
                if not gw.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.bot.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.bot.do(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, gw))
                        return

            # Boost Warpgate
            if self.bot.units(UnitTypeId.WARPGATE).ready.exists and self.bot.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                warps = self.bot.units(UnitTypeId.WARPGATE).ready.first
                if not warps.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.bot.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.bot.do(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, warps))
                        return

    async def createWall(self):
        """Wall-in Routine"""
        if self.bot.structures(UnitTypeId.PHOTONCANNON).ready.exists:
            cannons = self.bot.structures.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(10, self.bot.ramp_location)

            if self.bot.structures(UnitTypeId.PHOTONCANNON).ready.exists:
                shieldbattery = self.bot.structures.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(10, self.bot.ramp_location)

                if shieldbattery and cannons.amount >= 2:
                    return

        await self.bot.ressources.createGas()

        rampsupplybuildposition = self.bot.ramp_location
        if self.bot.structures(UnitTypeId.PYLON).ready.exists:
            rampsupply = self.bot.structures.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(5, rampsupplybuildposition)

            if rampsupply:
                self.ramp_supply_built = True
            else:
                self.ramp_supply_built = False
                await self.bot.unitmanager.createSupply(rampsupplybuildposition, 0, True)

            if self.ramp_supply_built:
                depot_placement_positions = self.bot.main_base_ramp.corner_depots | {self.bot.main_base_ramp.depot_in_middle}

                depots = self.bot.structures(UnitTypeId.PHOTONCANNON)

                if depots:
                    depot_placement_positions = {d for d in depot_placement_positions if depots.closest_distance_to(d) > 3}

                if len(depot_placement_positions) == 0:
                    return
                target_depot_location = depot_placement_positions.pop()
                await self.bot.unitmanager.createBuilding("PhotonCannon", 2, target_depot_location, 0, True)

                if depots.ready:
                    await self.bot.unitmanager.createBuilding("ShieldBattery", 1, rampsupplybuildposition, 7, True)