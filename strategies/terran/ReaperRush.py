from sc2.constants import *
from sc2 import race_worker
from sc2 import race_gas
from terran_micro import *


class ReaperRush:
    """Terran ReaperRush"""
    def __init__(self, bot=None):
        self.bot = bot
        self.ramp_supply_built = False
        self.workerscout = None
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.attack_count = 20
            self.bot.retreat_count = 3
            self.bot.current_strategy = 'ReaperRush'
            self.bot.squadsize = 10

            self.bot.not_squadunit.append(UnitTypeId.MEDIVAC)
            self.bot.not_commanderunit.append(UnitTypeId.MEDIVAC)

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: ReaperRush", team_only=True)

        await self.Macro()
        await self.Micro()
        await self.Buffs()

    async def Macro(self):
        """Macro Routine"""
        await self.bot.ressources.createGas()

        await self.createWall()
        await self.bot.unitmanager.trainSkill("SCANNERSWEEP_SCAN")

        barrackscount = self.bot.townhalls.amount * 2
        if barrackscount == 0:
            barrackscount = 1

        await self.bot.unitmanager.createBuilding("Barracks", barrackscount, self.bot.hq_location, 15)
        await self.bot.unitmanager.createUnit("Reaper", 10)
        await self.bot.unitmanager.createUnit("Marine", 50)
        await self.bot.unitmanager.createUnit("Medivac", 5)
        await self.bot.unitmanager.createUnit("Hellion", 10)

        # Check if expansion is needed
        if (self.bot.units(race_worker[self.bot.race]).ready.amount / 1.1) >= self.bot.neededworkers:
            await self.bot.warmanager.expand()

        if int(self.bot.time) >= 180 and self.bot.townhalls.amount == 1:
            await self.bot.warmanager.expand()
        if int(self.bot.time) >= 360 and self.bot.townhalls.amount <= 3:
            await self.bot.warmanager.expand()

    async def Micro(self):
        """Micro Routine"""
        tm = TerranMicro(self.bot)

        # Reaper
        reapers = self.bot.units(UnitTypeId.REAPER).ready
        if reapers:
            for reaper in reapers:
                await tm.Ground(reaper, True, True)

        # marine
        marines = self.bot.units(UnitTypeId.MARINE).ready
        if marines:
            for marine in marines:
                await tm.Ground(marine, True, True)

        # hellion
        hellions = self.bot.units(UnitTypeId.HELLION).ready
        if hellions:
            for hellion in hellions:
                await tm.Ground(hellion, True, True)

        # medivac
        medivacs = self.bot.units(UnitTypeId.MEDIVAC).ready
        if medivacs:
            for medivac in medivacs:
                if self.bot.commanderposition:
                    distance2commander = medivac.position.distance_to(self.bot.commanderposition)
                    if distance2commander >= 5:
                        self.bot.do(medivac.move(self.bot.commanderposition))


    async def Buffs(self):
        # Supply Depots
        # Raise depos when enemies are nearby
        for depo in self.bot.structures(UnitTypeId.SUPPLYDEPOT).ready:
            for unit in self.bot.enemy_units.not_structure:
                if unit.position.to2.distance_to(depo.position.to2) < 15:
                    break
            else:
                self.bot.do(depo(AbilityId.MORPH_SUPPLYDEPOT_LOWER))

        # Lower depos when no enemies are nearby
        for depo in self.bot.structures(UnitTypeId.SUPPLYDEPOTLOWERED).ready:
            for unit in self.bot.enemy_units.not_structure:
                if unit.position.to2.distance_to(depo.position.to2) < 10:
                    self.bot.do(depo(AbilityId.MORPH_SUPPLYDEPOT_RAISE))
                    break

        # Scanner Sweep
        for hq in self.bot.structures(UnitTypeId.ORBITALCOMMAND).ready.idle:
            abilities = await self.bot.get_available_abilities(hq)
            if AbilityId.SCANNERSWEEP_SCAN in abilities:
                self.bot.do(hq(AbilityId.SCANNERSWEEP_SCAN, self.bot.enemy_start_locations[0]))
                return

    async def createWall(self):
        depot_placement_positions = self.bot.main_base_ramp.corner_depots | {self.bot.main_base_ramp.depot_in_middle}

        depots = self.bot.structures(UnitTypeId.SUPPLYDEPOT) | self.bot.structures(UnitTypeId.SUPPLYDEPOTLOWERED)

        if depots:
            depot_placement_positions = {d for d in depot_placement_positions if depots.closest_distance_to(d) > 1}

        if self.bot.can_afford(UnitTypeId.SUPPLYDEPOT) and not self.bot.already_pending(UnitTypeId.SUPPLYDEPOT):
            if len(depot_placement_positions) == 0:
                return
            # Choose any depot location
            target_depot_location = depot_placement_positions.pop()
            ws = self.bot.workers.gathering
            if ws:  # if workers were found
                w = ws.random
                self.bot.do(w.build(UnitTypeId.SUPPLYDEPOT, target_depot_location))