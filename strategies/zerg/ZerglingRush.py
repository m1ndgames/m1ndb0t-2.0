from sc2.constants import *
from sc2 import race_worker
from sc2 import race_gas
from zerg_micro import *


class ZerglingRush:
    """Zerg ZerglingRush"""
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.current_strategy = 'ZerglingRush'
            self.bot.squadsize = 6
            self.bot.attack_count = 12
            self.bot.retreat_count = 2

            self.bot.not_squadunit.append(UnitTypeId.QUEEN)
            self.bot.not_commanderunit.append(UnitTypeId.QUEEN)

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: ZerglingRush", team_only=True)

        await self.Macro()
        await self.Micro()

    async def Macro(self):
        """Macro Routine"""
        if not self.bot.structures(race_gas[self.bot.race]):
            await self.bot.ressources.createGas()

        await self.bot.unitmanager.trainSkill("RESEARCH_ZERGLINGMETABOLICBOOST")

        await self.bot.unitmanager.createUnit("Zergling", 100)

        if self.bot.townhalls.ready.amount >= 3:
            await self.bot.unitmanager.createUnit("Queen", self.bot.townhalls.ready.amount)

        await self.bot.warmanager.expand()

        await self.bot.warmanager.scout()

    async def Micro(self):
        zm = ZergMicro(self.bot)

        # zergling
        zerglings = self.bot.units(UnitTypeId.ZERGLING).ready
        if zerglings:
            for zergling in zerglings:
                await zm.Ground(zergling)

        # queen
        queens = self.bot.units(UnitTypeId.QUEEN).ready
        if queens:
            for queen in queens:
                nearhq = self.bot.townhalls.closer_than(10, queen.position)
                if nearhq:
                    abilities = await self.bot.get_available_abilities(queen)
                    if AbilityId.EFFECT_INJECTLARVA in abilities:
                        self.bot.do(queen(AbilityId.EFFECT_INJECTLARVA, nearhq[0]))
                else:
                    hqs = self.bot.townhalls
                    for hq in hqs:
                        nearqueen = queens.closer_than(5, hq)
                        if nearqueen:
                            hqs.remove(hq)

                    if hqs:
                        self.bot.do(queen.move(hqs[0].position))
                        break
