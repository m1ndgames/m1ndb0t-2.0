from sc2.constants import *
from sc2 import race_worker
from sc2 import race_gas
from zerg_micro import *


class RoachSpam:
    """Zerg RoachSpam"""
    def __init__(self, bot=None):
        self.bot = bot
        self.bot.squadsize = 0
        self.bot.attack_count = 0
        self.bot.retreat_count = 0

    async def Run(self):
        """Runs on every GameStep"""
        if self.bot.iteration == 1:
            self.bot.current_strategy = 'RoachSpam'
            self.bot.squadsize = 10
            self.bot.attack_count = 10
            self.bot.retreat_count = 0

            self.bot.not_squadunit.append(UnitTypeId.QUEEN)
            self.bot.not_commanderunit.append(UnitTypeId.QUEEN)

            if self.bot.usechat:
                await self.bot._client.chat_send("Strategy: RoachSpam", team_only=True)

        await self.Macro()
        await self.Micro()

    async def Macro(self):
        """Macro Routine"""
        await self.bot.unitmanager.createUnit("Roach", 100)

        roachs = self.bot.units(UnitTypeId.ROACH).ready
        if self.bot.vespene <= 300:
            await self.bot.ressources.createGas()

        if self.bot.townhalls.ready.amount >= 3:
            await self.bot.unitmanager.createUnit("Queen", self.bot.townhalls.ready.amount)

        await self.bot.warmanager.expand()

        if self.bot.time >= 300:
            await self.bot.warmanager.scout()

    async def Micro(self):
        zm = ZergMicro(self.bot)

        # Roach
        roachs = self.bot.units(UnitTypeId.ROACH).ready
        if roachs:
            for roach in roachs:
                await zm.Ground(roach, True, True)

        # queen
        queens = self.bot.units(UnitTypeId.QUEEN).ready
        if queens:
            for queen in queens:
                nearhq = self.bot.townhalls.closer_than(10, queen.position)
                if nearhq:
                    abilities = await self.bot.get_available_abilities(queen)
                    if AbilityId.EFFECT_INJECTLARVA in abilities:
                        self.bot.do(queen(AbilityId.EFFECT_INJECTLARVA, nearhq[0]))
                else:
                    hqs = self.bot.townhalls
                    for hq in hqs:
                        nearqueen = queens.closer_than(5, hq)
                        if nearqueen:
                            hqs.remove(hq)

                    if hqs:
                        self.bot.do(queen.move(hqs[0].position))
                        break
