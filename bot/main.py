from m1ndb0t import m1ndb0t
import json
from pathlib import Path

class MyBot(m1ndb0t):
    with open(Path(__file__).parent / "../botinfo.json") as f:
        NAME = json.load(f)["name"]