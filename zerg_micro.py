from sc2.constants import *


class ZergMicro:
    """Zerg Micro"""
    def __init__(self, bot=None):
        self.bot = bot

    async def Ground(self, unit, kite=False, support=False):
        if not unit:
            return

        unit_abilities = await self.bot.get_available_abilities(unit)
        enemie_units = self.bot.enemy_units
        enemie_structures = self.bot.enemy_structures

        if support:
            if self.bot.commandertarget and unit.tag != self.bot.commander:
                if unit.position.distance_to(self.bot.commandertarget.position) <= self.bot.techtree.get_unit_weaponrange(unit.type_id):
                    self.bot.do(unit.attack(self.bot.commandertarget))
                    return

        if enemie_units:
            closeenemies = enemie_units.closer_than(30, unit)
            if closeenemies:
                for ce in closeenemies:
                    if not self.bot.techtree.can_attack(unit, ce) or not ce.can_be_attacked or ce.name == 'Larva':
                        closeenemies.remove(ce)

            if closeenemies:
                next_enemy = closeenemies.closest_to(unit)
                distance2enemy = unit.distance_to(next_enemy)
                if kite:
                    if distance2enemy < self.bot.techtree.get_unit_weaponrange(unit.type_id):
                        if not next_enemy.position:
                            moveposition = unit.position.towards(self.bot.hq_location, 1)
                            if moveposition and self.bot.in_pathing_grid(moveposition):
                                self.bot.do(unit.move(moveposition))
                        else:
                            moveposition = unit.position.towards(next_enemy.position, -1)
                            if moveposition and self.bot.in_pathing_grid(moveposition):
                                self.bot.do(unit.move(moveposition))
                else:
                    if distance2enemy < 30:
                        self.bot.do(unit.attack(next_enemy))
                        return

        if enemie_structures:
            closestructures = enemie_structures.closer_than(30, unit)

            if closestructures:
                for cs in closestructures:
                    if not self.bot.techtree.can_attack(unit, cs) or not cs.can_be_attacked:
                        closestructures.remove(cs)

                if closestructures:
                    next_structure = closestructures.closest_to(unit)
                    if next_structure:
                        distance2structure = unit.position.distance_to(next_structure.position)
                        if distance2structure < 30:
                            self.bot.do(unit.attack(next_structure))
                            return

    async def Air(self, unit, kite=False, support=False):
        if not unit:
            return

        unit_abilities = await self.bot.get_available_abilities(unit)
        enemie_units = self.bot.enemy_units
        enemie_structures = self.bot.enemy_structures

        if support:
            if self.bot.commandertarget and unit.tag != self.bot.commander:
                if unit.position.distance_to(self.bot.commandertarget.position) <= self.bot.techtree.get_unit_weaponrange(unit.type_id):
                    self.bot.do(unit.attack(self.bot.commandertarget))
                    return

        if enemie_units:
            closeenemies = enemie_units.closer_than(30, unit)
            if closeenemies:
                for ce in closeenemies:
                    if not self.bot.techtree.can_attack(unit, ce) or not ce.can_be_attacked or ce.name == 'Larva':
                        closeenemies.remove(ce)

            if closeenemies:
                next_enemy = closeenemies.closest_to(unit)
                distance2enemy = unit.distance_to(next_enemy)
                if kite:
                    if distance2enemy < self.bot.techtree.get_unit_weaponrange(unit.type_id):
                        if not next_enemy.position:
                            moveposition = unit.position.towards(self.bot.hq_location, 1)
                            if moveposition and self.bot.in_pathing_grid(moveposition):
                                self.bot.do(unit.move(moveposition))
                        else:
                            moveposition = unit.position.towards(next_enemy.position, -1)
                            if moveposition and self.bot.in_pathing_grid(moveposition):
                                self.bot.do(unit.move(moveposition))
                else:
                    if distance2enemy < 30:
                        self.bot.do(unit.attack(next_enemy))
                        return

        if enemie_structures:
            closestructures = enemie_structures.closer_than(30, unit)

            if closestructures:
                for cs in closestructures:
                    if not self.bot.techtree.can_attack(unit, cs) or not cs.can_be_attacked:
                        closestructures.remove(cs)

                if closestructures:
                    next_structure = closestructures.closest_to(unit)
                    if next_structure:
                        distance2structure = unit.position.distance_to(next_structure.position)
                        if distance2structure < 30:
                            self.bot.do(unit.attack(next_structure))
                            return